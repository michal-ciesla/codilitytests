/**
 *
 */

public class Exe3 {
        int solution(int[] A) {
            int n = A.length;
            int result = 0;
            for (int i = 0; i < n - 1; i++) {
                if (A[i] == A[i + 1])
                    result = result + 1;
            }
            int r = 0;
            for (int i = 0; i < n; i++) {
                int count = 0;
               // System.out.println(i);
                if (i > 0) {
                    if (A[i - 1] != A[i])
                        count = count + 1;
                    else
                        count--;
                }
                if (i < n - 1) {
                    if (A[i + 1] != A[i])
                        count = count + 1;
                    else
                        count--;
                }
                r = Math.max(r, count);
               // if(r == 2) break;
        }
            return result + r;
        }
}
