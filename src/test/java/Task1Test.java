import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class Task1Test {

    @Test
    public void solution() throws Exception {
        System.out.println("test 1");
        Task1 task1 = new Task1();
        int[] A = {1,1,6};
        int minSteps = task1.solution(A);
        System.out.println("*********************");
        Assert.assertEquals("Minimal steps should be: ", 2, minSteps);
    }

    @Test
    public void solution2() throws Exception {
        System.out.println("test 2");
        Task1 task1 = new Task1();
        int[] A = {1,2,3};
        int minSteps = task1.solution(A);
        System.out.println("*********************");
        Assert.assertEquals("Minimal steps should be: ", 2, minSteps);
    }

    @Test
    public void solution3() throws Exception {
        System.out.println("test 3");
        Task1 task1 = new Task1();
        int[] A = {0};
        int minSteps = task1.solution(A);
        System.out.println("*********************");
        Assert.assertEquals("Minimal steps should be: ", 0, minSteps);
    }

    @Test
    public void solution4() throws Exception {
        System.out.println("test 4");
        Task1 task1 = new Task1();
        int[] A = {};
        int minSteps = task1.solution(A);
        System.out.println("*********************");
        Assert.assertEquals("Minimal steps should be: ", 0, minSteps);
    }

    @Test
    public void solution5() throws Exception {
        System.out.println("test 5");
        Task1 task1 = new Task1();
        int[] A = {1,2,5,3,4,5,1};
        int minSteps = task1.solution(A);
        System.out.println("*********************");
        Assert.assertEquals("Minimal steps should be: ", 5, minSteps);
    }

    @Test
    public void solution6() throws Exception {
        System.out.println("test 6");
        Task1 task1 = new Task1();
        int[] A = {1,2,5,3,4,5,1,6,2,1,1,1,1,3,4};
        int minSteps = task1.solution(A);
        System.out.println("*********************");
        Assert.assertEquals("Minimal steps should be: ", 10, minSteps);
    }

    @Test
    public void solution7() throws Exception {
        System.out.println("test 7");
        Task1 task1 = new Task1();
        int[] A = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
        int minSteps = task1.solution(A);
        System.out.println("*********************");
        Assert.assertEquals("Minimal steps should be: ", 0, minSteps);
    }

    @Test
    public void solution8() throws Exception {
        System.out.println("test 8");
        Task1 task1 = new Task1();
        int[] A = {6,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
        int minSteps = task1.solution(A);
        System.out.println("*********************");
        Assert.assertEquals("Minimal steps should be: ", 2, minSteps);
    }

}