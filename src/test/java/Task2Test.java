import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class Task2Test {

    @Test
    public void solution() throws Exception {
        Task2 task2 = new Task2();
        String S = "(())";
        int K = task2.solution(S);
        Assert.assertEquals("K should be: ", 2, K);
    }

    @Test
    public void solution2() throws Exception {
        Task2 task2 = new Task2();
        String S = "(())))(";
        int K = task2.solution(S);
        Assert.assertEquals("K should be: ", 4, K);
    }

    @Test
    public void solution3() throws Exception {
        Task2 task2 = new Task2();
        String S = "))";
        int K = task2.solution(S);
        Assert.assertEquals("K should be: ", 2, K);
    }

    @Test
    public void solution4() throws Exception {
        Task2 task2 = new Task2();
        String S = "";
        int K = task2.solution(S);
        Assert.assertEquals("K should be: ", 0, K);
    }

}